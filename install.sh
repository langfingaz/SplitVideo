#!/bin/bash

# installation directory (should be included in your PATH variable)
PROGRAM_DIR="/usr/local/bin"
# base path where files to install are located
FILE_LOCATION="."
# names of files to install
# The LAST file must be the executable (with GUI)
FILE_NAMES=( "splitVideo" "splitVideoGUI" )
# .desktop directory 
DESKTOP_DIR="/usr/share/applications"



function createDesktopEntry(){
  
  # .desktop categories:
  # https://specifications.freedesktop.org/menu-spec/latest/apa.html#main-category-registry
  
  printf "Creating desktop entry for executable %s\n" "$PROGRAM_DIR/${FILE_NAMES[-1]}"
    
cat <<EOF > "$DESKTOP_DIR/${FILE_NAMES[-1]}.desktop"
[Desktop Entry]
Version=1.0
Name=SplitVideo
Comment=Splits a video up in two parts at specific time
Categories=AudioVideo;Video;Utility;
Exec=${FILE_NAMES[-1]}
Icon=yad
Terminal=false
Type=Application
StartupNotify=true
EOF
}

function validateDesktopEntry(){
  printf "\n========= Validating Desktop Entry =========\n\n"
  desktop-file-validate "$DESKTOP_DIR/${FILE_NAMES[-1]}.desktop"
  printf "========= ======================== =========\n\n"
}

function main(){
  # copy files to /usr/local/bin
  # and make them executable

  for FILE_NAME in "${FILE_NAMES[@]}"; do
    cp "$FILE_LOCATION/$FILE_NAME" "$PROGRAM_DIR" || exit "$?"
    chmod +x "$PROGRAM_DIR/$FILE_NAME" || exit "$?"
  done
  
  createDesktopEntry || exit "$?"

  # update desktop entries
  # https://wiki.archlinux.org/index.php/Desktop_entries#Update_database_of_desktop_entries
  update-desktop-database "$DESKTOP_DIR"
  
  # validate desktop entry
  # https://wiki.archlinux.org/index.php/Desktop_entries#Validation
  validateDesktopEntry

  echo "Installation was successfull"
  return 0
}

main "$@"
