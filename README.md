# SplitVideo

## Installation

Read and understand the ![install.sh](install.sh) installation script. Then run

```
sudo ./install.sh
```

## Usage

Start the app via desktop entry:

![](screenshot_start-app.jpeg)

In the main window, select a source video and enter the time at which you want to split the video into two pieces. The original file will not be modified.

![](screenshot_main-gui.jpeg)

You will be informed in a small message window when the conversation is done and there will be two new files (with an appended prefix) next to your source video:

![](screenshot_success.jpeg)

## TODO

* `splitVideoGUI`
	- ckeck if `yad` is installed
* `splitVideo`
	- check if `ffmpeg` is installed
	
* check dependencies in `install.sh`
